# Constants
S=src
O=bin
H=inc
CC=gcc
CFLAGS=-W -Werror -Wall
LDFLAGS=
SDL_CFLAGS:=$(shell pkg-config --cflags sdl2 SDL2_mixer)
SDL_LDFLAGS:=$(shell pkg-config --libs sdl2 SDL2_mixer)
override CFLAGS += $(SDL_CFLAGS)
override LDFLAGS += $(SDL_LDFLAGS)
EXEC=Platformer

# Executables
all: $(EXEC)

Platformer: $(O)/main.o
	$(CC) -o $@ $^ $(LDFLAGS)

# Binaries
$(O)/main.o: $(S)/main.c $(H)/main.h
	$(CC) -c $(S)/main.c -o $@ $(CFLAGS)

# Clean & others	
clean:
	rm -rf $(O)/*.o

mrproper: clean
	rm -rf $(EXEC)

.PHONY: clean mrproper
