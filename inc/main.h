#ifndef _MAIN_H_
#define _MAIN_H_
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

bool analyze_events(SDL_Window* pWindow,SDL_Event event);
#endif
