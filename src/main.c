#include "../inc/main.h"

/*	Take true when the window is in full screen and false otherwise.	*/
bool IS_FULL_SCREEN = false;

/*
 *	Contains the main loop of the program.
 * */
int main(void){
	if(SDL_Init(SDL_INIT_VIDEO) != 0){
		fprintf(stderr,"SDL Initialization Error (%s)\n",SDL_GetError());
		return -1;
	}
	SDL_Window* pWindow = NULL;
	pWindow = SDL_CreateWindow("SDL2 Test",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,640,480,SDL_WINDOW_SHOWN);
	if(!pWindow){
		fprintf(stderr,"Window Creation Error (%s)\n",SDL_GetError());
		return -1;
	}
	SDL_Event event;
	while(SDL_PollEvent(&event) || analyze_events(pWindow,event)){/*MAIN LOOP*/}
	SDL_DestroyWindow(pWindow);
	SDL_Quit();
	return 0;
}

/*
 * Return false if the event to quit is encountered.
 * True otherwise.
 * */
bool analyze_events(SDL_Window* pWindow, SDL_Event event){	
	switch(event.type){
		case SDL_QUIT:
			return false;
		case SDL_KEYUP:
			if(event.key.keysym.sym == SDLK_F12){
				if(!IS_FULL_SCREEN){
					IS_FULL_SCREEN = true;
					SDL_SetWindowFullscreen(pWindow,SDL_WINDOW_FULLSCREEN);
				}else{
				  	IS_FULL_SCREEN = false;
					SDL_SetWindowFullscreen(pWindow,0);
				}
			}
			break;
	}
	return true;
}
